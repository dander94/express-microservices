## Express Messages

### Description
Simple project for training.
It's a microservices architecture using queues for communication between services.

![](/docs/Sample.png)

Tech Stack:
- Chore: Docker Compose, Make
- Backend: Express, Typescript, Knex, Socket.IO
- Frontend: Vite, React, React Router, Typescript, Socket.IO, Tailwind
- DB: PostgreSQL
- Queues: RabbitMQ


### Messages
Allow sending a message using and endpoint. The message will be sent to Server using RabbitMQ.
- Tech: TypeScript, Express, RabbitMQ
- Usage:
```
POST localhost:3001/message/send/

Request:
{ "from": "+17175550196", "to": "+17175550195", "content": "Test" }

Response:
{ "success": bool }
```


### Server
Contacts & Messages application. Creates the objects in the DB. Allows querying contacts and messages. Allows creating a new contact. Notifies new content created.
- Tech: TypeScript, Express, RabbitMQ, Knex, PostgreSQL
- Usage:
```
Login into the system to get the user ID. If no user exists, it's created.
URL: /auth/login/

POST Request:
{ "phoneNumber": "+12025550196" }

Response:
User with ID and Phone Number

--------------------------------

List contacts for user
URL: /contacts/contact/

GET Request.
- Include header: { "user-id": <user_id> }

Response
- Array of contacts

--------------------------------

Create contact for user
URL: /contacts/contact/

POST Request.
- Include header: { "user-id": <user_id> }
{ "fullName": "John Doe", "phoneNumber": "+12025550195" }

Response
- New contact

--------------------------------

List messages for user
URL: /messages/message/

GET Request.
- Include header: { "user-id": <user_id> }

Response
- Array of messages
```

### Notifications
Receives events from server using RabbitMQ and send them to the frontend using Socket.io
- Tech: TypeScript, Express, RabbitMQ, Socket.IO


### Frontend

Frontend application using React. UI to login, manage contacts, and send messages.
- Tech: TypeScript, Vite, React, React Router, Socket.IO, Tailwind Css


### Running the project

#### Envs
Create a .env in the root of the project and set the appropiate values. You can use the template `.env.default`

```
cp .env.default .env
```

Create a .env for the messages application. Again, you can use the template `.env.default`

```
cd messages
cp .env.default .env
cd ..
```

Create a .env for the contacts application. Again, you can use the template `.env.default`
```
cd server
cp .env.default .env
cd ..
```

Create a .env for the notifications application. Again, you can use the template `.env.default`
```
cd notifications
cp .env.default .env
cd ..
```

#### Run & Stop
To start it use:
 `make start`

 To stop it use:
 `make stop`

The application will be shown in the URL: `http://localhost:5173/`

### Migrations

#### Create Migration
```
make migrate.make NAME=<migration_name>
```

### Apply all migrations
```
make migrate.latest
```

#### Apply next migration
```
make migrate.up
```

#### Unapply last migration
```
make migrate.down
```

## Future features:
- Conversations instead of all messages in a single page
- Tests
- Auth

start:
	@echo "Running project..."
	@docker-compose -f docker-compose.yml  up -d

stop:
	@echo "Stopping project..."
	@docker-compose -f docker-compose.yml down --remove-orphans

migrate.make:
	@echo "Creating migration ${NAME}"
	@docker exec -it express-contacts-1 /bin/sh -c "cd /server && npx knex migrate:make ${NAME}"

migrate.up:
	@echo "Applying next migration"
	@docker exec -it express-contacts-1 /bin/sh -c "cd /server && npx knex migrate:up"

migrate.latest:
	@echo "Applying all pending migrations"
	@docker exec -it express-contacts-1 /bin/sh -c "cd /server && npx knex migrate:latest"


migrate.down:
	@echo "Unapplying last migration"
	@docker exec -it express-contacts-1 /bin/sh -c "cd /server && npx knex migrate:down"

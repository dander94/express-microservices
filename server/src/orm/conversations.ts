import { DBConversation } from '../@types/conversations';
import { database } from '../database';

export const getOrCreateConversation: (number1: string, number2: string) => Promise<DBConversation> = async (number1, number2) => {
  let conversation: DBConversation | undefined;

  let [dbConv]: { id: number, number_1: string, number_2: string, last_message_at: Date }[] = await database('conversations')
    .select('id', 'number_1', 'number_2', 'last_message_at')
    .where({ number_1: number1, number_2: number2 })
    .orWhere({ number_2: number1, number_1: number2 })
    .limit(1);

  if (dbConv) {
    conversation = {
      id: dbConv.id,
      number1: dbConv.number_1,
      number2: dbConv.number_2,
      lastMessageAt: dbConv.last_message_at,
    }
  } else {
    const [{ id: newConversationId }]: { id: number }[] = await database('conversations')
      .returning('id')
      .insert({ number_1: number1, number_2: number2 });

    conversation = {
      id: newConversationId,
      number1,
      number2,
    }
  }

  return conversation
}

export const getAllUserConversations: (userId: number) => Promise<DBConversation[]> = async (userId) => {
  const conversations: { id: number, number_1: string, number_2: string, last_message_at: Date }[] = await database('conversations')
    .select('id', 'number_1', 'number_2', 'last_message_at')
    .union(
      database('users')
        .select('phoneNumber')
        .where('id', userId)
    )
    .where({ number_1: database.ref('phoneNumber') })
    .orWhere({ number_2: database.ref('phoneNumber') })
    .orderBy('last_message_at', 'desc');

  return conversations.map((c) => ({
    id: c.id,
    number1: c.number_1,
    number2: c.number_2,
    lastMessageAt: c.last_message_at,
  }));
}

export const updateConversationLastMessageAt: (conversationId: number, lastMessageAt: Date) => Promise<void> = async (conversationId, lastMessageAt) => {
  await database('conversations').update({ last_message_at: lastMessageAt }).where('id', conversationId);
}
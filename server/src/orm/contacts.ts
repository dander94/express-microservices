import { Contact, DBContact } from "../@types/contacts";

import { database } from '../database';

export const insertContact: (contact: Contact, userId: number) => Promise<DBContact> = async (contact, userId) => {
  const [{ id }]: { id: number }[] = await database('contacts')
    .returning('id')
    .insert({
      full_name: contact.fullName,
      phone_number: contact.phoneNumber,
      user_id: userId,
    });
  return {
    id,
    ...contact
  }
}

export const getAllUserContacts: (userId: number) => Promise<DBContact[]> = async (userId) => {
  const contacts: { id: number, full_name: string, phone_number: string }[] = await database('contacts').select('id', 'full_name', 'phone_number').where('user_id', userId)
  return contacts.map(({ id, full_name, phone_number }) => ({
    id,
    fullName: full_name,
    phoneNumber: phone_number,
  }))
}

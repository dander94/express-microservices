import { database } from '../database';

import { DBMessage, Message } from "../@types/messages";

export const insertMessage: (message: Message, conversationId: number) => Promise<DBMessage> = async (message, conversationId) => {
  const [{ id, created_at: createdAt }]: { id: number, created_at: Date }[] = await database('messages')
    .returning(['id', 'created_at'])
    .insert({ ...message, conversation_id: conversationId });
  return {
    id,
    createdAt,
    conversationId,
    ...message,
  }
}

export const getAllUserMessages: (userId: number) => Promise<DBMessage[]> = async (userId) => {
  const messages: { id: number, from: string, to: string, created_at: Date, content: string, conversation_id: number }[] = await database('messages as m')
    .select('m.id', 'm.from', 'm.to', 'm.created_at', 'm.content', 'm.conversation_id')
    .innerJoin(
      database('users')
        .select('id', 'phone_number')
        .where('id', userId)
        .limit(1)
        .as('u'),
      function () {
        this.on('m.from', '=', 'u.phone_number').orOn('m.to', '=', 'u.phone_number');
      }
    )
    .orderBy('m.created_at', 'asc');

  return messages.map((c) => ({
    id: c.id,
    from: c.from,
    to: c.to,
    createdAt: c.created_at,
    content: c.content,
    conversationId: c.conversation_id
  }));
}
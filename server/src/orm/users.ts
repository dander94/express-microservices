import { DBUser } from "../@types/users";

import { database } from '../database';

export const getUserByPhoneNumber: (phoneNumber: string) => Promise<DBUser | null> = async (phoneNumber) => {
  const users: { id: number, phone_number: string }[] = await database('users').select('id', 'phone_number').where('phone_number', phoneNumber).limit(1);
  if (!users.length) return null;

  return {
    id: users[0].id,
    phoneNumber: users[0].phone_number,
  }
}

export const getOrCreateUserByPhoneNumber: (phoneNumber: string) => Promise<DBUser> = async (phoneNumber) => {
  const user = await getUserByPhoneNumber(phoneNumber);

  if (user) return user;

  const [{ id: newUserId }]: { id: number }[] = await database('users').returning('id').insert({ phone_number: phoneNumber });
  return {
    id: newUserId,
    phoneNumber,
  }
};

export const getUsersByPhoneNumbers: (phoneNumbers: string[]) => Promise<DBUser[]> = async (phoneNumbers = []) => {
  const users: { id: number, phone_number: string }[] = await database('users').select('id', 'phone_number').whereIn('phone_number', phoneNumbers);

  return users.map(({ id, phone_number }) => ({
    phoneNumber: phone_number,
    id,
  }));
}
import { DBContact } from "../@types/contacts";
import { sendNotification } from "../amqp";

export const notifyContactCreated: (userId: number, contact: DBContact) => void = async (userId, contact) => {
  sendNotification({
    name: 'contact:created',
    payload: contact,
    userId,
  });
}
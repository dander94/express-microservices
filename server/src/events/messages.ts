import { DBMessage, Message } from "../@types/messages";
import { EventNotification } from "../@types/notifications";
import { sendNotification } from "../amqp";
import { getOrCreateConversation, updateConversationLastMessageAt } from "../orm/conversations";
import { insertMessage } from "../orm/messages";
import { getUsersByPhoneNumbers } from "../orm/users";

export const onMessageReceived: (message: Message) => Promise<void> = async (message) => {
  const conversation = await getOrCreateConversation(message.from, message.to);
  const dbMessage = await insertMessage(message, conversation.id);
  updateConversationLastMessageAt(conversation.id, dbMessage.createdAt);
  notifyMessageCreated(dbMessage);
}

export const notifyMessageCreated: (message: DBMessage) => void = async (message) => {
  const users = await getUsersByPhoneNumbers([message.from, message.to]);

  users.forEach(({ id }) => {
    sendNotification({
      name: 'message:created',
      userId: id,
      payload: message,
    });
  });
}
import type { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable('messages', function (table) {
    table.increments('id');
    table.string('from', 12).notNullable().index();
    table.string('to', 12).notNullable().index();
    table.string('content', 500).notNullable();
    table.timestamp('created_at').defaultTo(knex.fn.now()).notNullable();
    table.integer('conversation_id').unsigned().notNullable();
    table.foreign('conversation_id').references('conversations.id').onDelete('CASCADE');
  })
}


export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable('messages')
}

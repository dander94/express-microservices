import type { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable('contacts', function (table) {
    table.increments('id');
    table.string('full_name', 255).notNullable();
    table.string('phone_number', 12).notNullable();
    table.integer('user_id').unsigned();
    table.foreign('user_id').references('users.id').onDelete('CASCADE');
    table.unique(['user_id', 'phone_number']);
  })
}


export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable('contacts')
}


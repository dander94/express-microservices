import type { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable('users', function (table) {
    table.increments('id');
    table.string('phone_number', 12).notNullable().unique();
  })
}


export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable('users')
}


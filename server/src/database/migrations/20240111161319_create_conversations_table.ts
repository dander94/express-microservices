import type { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable('conversations', function (table) {
    table.increments('id');
    table.string('number_1', 12).notNullable().index();
    table.string('number_2', 12).notNullable().index();
    table.timestamp('last_message_at').defaultTo(knex.fn.now());
  })
}


export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable('conversations')
}

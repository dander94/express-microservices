import knex from 'knex';
import configs from '../../knexfile'

export const database = knex(configs[process.env.NODE_ENV || 'development']);

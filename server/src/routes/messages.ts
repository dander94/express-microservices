import express from 'express';
import { requireUser } from '../middlewares/users';
import { RequestHandler } from 'express-serve-static-core';
import { getAllUserMessages } from '../orm/messages';

const router = express.Router();

const getUserMessages: RequestHandler = async (req, res) => {
  const userId: string = req.header('user-id')!;

  res.send(await getAllUserMessages(parseInt(userId)));
};

router.route('/message/')
  .get([requireUser, getUserMessages]);

module.exports = router
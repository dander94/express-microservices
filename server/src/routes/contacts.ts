import express from 'express';
import { requireUser } from '../middlewares/users';
import { RequestHandler } from 'express-serve-static-core';
import { getAllUserContacts, insertContact } from '../orm/contacts';
import { Contact } from '../@types/contacts';
import { notifyContactCreated } from '../events/contacts';

const router = express.Router();

const getContacts: RequestHandler = async (req, res) => {
  const userId: string = req.header('user-id')!;

  res.send(await getAllUserContacts(parseInt(userId)));
};

const createContact: RequestHandler = async (req, res) => {
  const userId: string = req.header('user-id')!;

  const contact = (req.body as Contact);

  // TODO: More validation (format, unique...)
  if (!contact.fullName || !contact.phoneNumber) {
    res.send('Full Name and Phone Number are required');
    return
  }

  const newContact = await insertContact(contact, parseInt(userId));
  notifyContactCreated(parseInt(userId), newContact);

  res.send(newContact);
};

router.route('/contact/')
  .get([requireUser, getContacts])
  .post([requireUser, createContact]);

module.exports = router
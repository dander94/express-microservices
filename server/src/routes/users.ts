import express from 'express';
import { getOrCreateUserByPhoneNumber } from '../orm/users';

const router = express.Router();

/*
Very stupid endpoint that will get or create an user by using the phone number. Complex auth will come at some point
*/
router.post('/login/', async (req, res) => {
  const { phoneNumber } = (req.body as { phoneNumber: string });

  const user = await getOrCreateUserByPhoneNumber(phoneNumber);

  res.send(user);
});

module.exports = router
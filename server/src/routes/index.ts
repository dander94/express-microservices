import express from 'express';

const router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
  res.send('Hello World');
});

router.use('/auth/', require('./users'));
router.use('/contacts/', require('./contacts'));
router.use('/messages/', require('./messages'));

module.exports = router

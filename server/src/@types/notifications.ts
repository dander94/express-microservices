export interface EventNotification {
  name: string,
  userId: number,
  payload: Object,
}
import { DBModel } from "./db"

export interface User {
  phoneNumber: string
};

export type DBUser = DBModel<User>;

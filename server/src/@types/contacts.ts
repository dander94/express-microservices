import { DBModel } from "./db";

export interface Contact {
  fullName: string;
  phoneNumber: string;
};

export type DBContact = DBModel<Contact>

export type CreateContactInput = Contact

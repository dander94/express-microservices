import { DBModel } from "./db";

export interface Message {
  from: string,
  to: string,
  content: string,
};

export type DBMessage = DBModel<Message> & { conversationId: number, createdAt: Date };
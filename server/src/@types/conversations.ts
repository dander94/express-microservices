import { DBModel } from "./db";

export interface Conversation {
  number1: string,
  number2: string,
  lastMessageAt?: Date,
}

export type DBConversation = DBModel<Conversation>

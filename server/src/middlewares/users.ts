import { RequestHandler } from 'express';

export const requireUser: RequestHandler = (req, res, next) => {
  const userId = req.header('user-id');

  if (!userId) {
    res.status(401).send();
  } else {
    next();
  }
}
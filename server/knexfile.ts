import dotenv from 'dotenv'
import type { Knex } from 'knex';

dotenv.config();

const environments = ['development', 'staging', 'production'] as const;

const connection: Knex.ConnectionConfig = {
  database: process.env.POSTGRES_DB as string,
  host: process.env.POSTGRES_HOST as string,
  password: process.env.POSTGRES_PASSWORD as string,
  user: process.env.POSTGRES_USER as string,
};

export const dbConfig: Knex.Config = {
  client: 'pg',
  connection,
  pool: {
    min: 2,
    max: 10,
  },
  migrations: {
    directory: './src/database/migrations',
    extension: 'ts',
    tableName: 'knex_migrations',
  }
};

export default environments.reduce((acc, env) => ({ ...acc, [env]: dbConfig }), {}) as Record<string, Knex.Config>
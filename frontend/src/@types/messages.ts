export interface Message {
  id: number;
  from: string;
  to: string;
  content: string;
  createdAt: Date;
};
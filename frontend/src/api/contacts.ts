import { Contact } from "../@types/contacts";

export const getAllUserContacts: (userId: number) => Promise<Contact[]> = async (userId) => {
  const response = await fetch('http://localhost:3000/contacts/contact/', {
    headers: {
      'user-id': `${userId}`,
    },
  });

  const data: Contact[] = await response.json();
  return data;
};

export const createContact: (userId: number, fullName: string, phoneNumber: string) => Promise<Contact> = async (userId, fullName, phoneNumber) => {
  const response = await fetch('http://localhost:3000/contacts/contact/', {
    body: JSON.stringify({ fullName, phoneNumber }),
    headers: {
      'Content-Type': 'application/json',
      'user-id': `${userId}`,
    },
    method: 'POST',
  });

  return await response.json() as Contact;
};
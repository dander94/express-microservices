import { User } from "../@types/users";

export const login: (phoneNumber: string) => Promise<User> = async (phoneNumber) => {
  const response = await fetch('http://localhost:3000/auth/login/', {
    body: JSON.stringify({ phoneNumber }),
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'POST',
  });

  const data: User = await response.json();
  return data;
};
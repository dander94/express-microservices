import { Message } from "../@types/messages";

export const getAllUserMessages: (userId: number) => Promise<Message[]> = async (userId) => {
  const response = await fetch('http://localhost:3000/messages/message/', {
    headers: {
      'user-id': `${userId}`,
    },
  });

  const data: (Message & { createdAt: string })[] = await response.json();
  return data.map((message) => ({ ...message, createdAt: new Date(message.createdAt) }));
};

export const sendMessage: (from: string, to: string, content: string) => Promise<boolean> = async (from, to, content) => {
  const response = await fetch('http://localhost:3001/message/send/', {
    body: JSON.stringify({ from, to, content }),
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'POST',
  });

  const { success } = await response.json() as { success: boolean };

  return success;
}
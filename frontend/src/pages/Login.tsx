import React, { useContext, useState } from 'react'

import { useNavigate } from 'react-router-dom';

import { login } from '../api/auth';

import UserContext, { UserContextType } from './../context/users';
import Card from '../components/ui/Card';
import Button from '../components/ui/Button';
import Input from '../components/forms/Input';

function Login() {
  const navigate = useNavigate();
  const { setUser } = useContext<UserContextType>(UserContext);
  const [phoneNumber, setPhoneNumber] = useState<string>('');

  const doLogin = async () => {
    if (!phoneNumber) return;

    const user = await login(phoneNumber);

    setUser(user);
    navigate('/');
  }

  return (
    <Card className="w-[20rem] mx-auto mt-[10rem]">
      <p className="text-lg font-bold mb-3">Hi!</p>
      <p>Enter your phone number to begin</p>

      <Input
        className="w-full mt-4"
        placeholder="+12025550196"
        type="text"
        onChange={(e) => setPhoneNumber((e.target as HTMLInputElement).value)}
      />
      <Button
        className="w-full mt-2"
        onClick={doLogin}
      >
        Log in
      </Button>
    </Card>
  )
}

export default Login

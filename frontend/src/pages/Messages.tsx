import React, { useContext, useEffect, useRef } from 'react';

import { Message } from '../@types/messages';

import ContactContext, { ContactContextType } from '../context/contacts';
import MessageContext, { MessageContextType } from '../context/messages';
import UserContext, { UserContextType } from '../context/users';
import UIMessage from '../components/ui/Message';

function Messages() {
  const { contacts } = useContext<ContactContextType>(ContactContext);
  const { messages } = useContext<MessageContextType>(MessageContext);
  const { user } = useContext<UserContextType>(UserContext);

  const isByUser = ({ from }: Message) => from == user!.phoneNumber

  const getContactName = (message: Message) => {
    const contactPhone = (isByUser(message)) ? message.to : message.from
    return contacts.find(({ phoneNumber }) => phoneNumber === contactPhone)?.fullName || contactPhone
  }

  const messagesEndRef = useRef<HTMLDivElement>(null);

  const scrollToBottom = (behavior?: ScrollBehavior) => {
    messagesEndRef.current?.scrollIntoView(behavior ? ({ behavior: behavior }) : undefined)
  }

  useEffect(() => {
    scrollToBottom();
  }, [messages]);


  return (
    <div className="flex flex-col w-full px-[5vw]">
      {
        messages.map(
          (m) => (
            <UIMessage
              key={m.id}
              className={`${isByUser(m) ? 'self-end' : 'self-start'} max-w-[30vw] mb-5`}
              contactName={getContactName(m)}
              isSentByUser={isByUser(m)}
              messageDate={m.createdAt}
            >
              {m.content}
            </UIMessage>
          )
        )
      }
      <div ref={messagesEndRef} />
    </div>
  )
}

export default Messages

import { io } from 'socket.io-client';
import { Message } from './@types/messages';
import { Contact } from './@types/contacts';

export const initSocket: (
  userId: number,
  onReceived: (type: 'message:created' | 'contact:created', data: object) => void,
) => void = (userId, onReceived) => {
  const socket = io(`ws://localhost:3002?user_id=${userId}`);
  socket.on('message:created', (data) => onReceived('message:created', data));
  socket.on('contact:created', (data) => onReceived('contact:created', data));
}
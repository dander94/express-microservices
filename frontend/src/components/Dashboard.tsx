import React, { PropsWithChildren, useContext, useEffect, useState } from "react"

import ContactContext from "../context/contacts";
import MessageContext from "../context/messages";
import UserContext, { UserContextType } from '../context/users';

import { createContact, getAllUserContacts } from '../api/contacts';

import Sidebar from "./dashboard/Sidebar";
import Contacts from "./dashboard/Contacts";
import { Contact } from "../@types/contacts";
import Button from "./ui/Button";
import AddContactModal from "./modals/AddContactModal";
import SendMessageModal from "./modals/SendMessageModal";
import { getAllUserMessages, sendMessage } from "../api/messages";
import NotificationHandler from "./NotificationHandler";
import { Message } from "../@types/messages";

function Dashboard(props: PropsWithChildren<{}>) {
  const { children } = props;
  const { user } = useContext<UserContextType>(UserContext);


  const [contacts, setContacts] = useState<Contact[]>([]);
  const [messages, setMessages] = useState<Message[]>([]);

  const [isSendMessageModalVisible, setIsSendMessageModalVisible] = useState(false);
  const [sendMessageModalProps, setSendMessageModalProps] = useState({ contactName: '', initialTo: '' });

  const [isAddContactModalVisible, setIsAddContactModalVisible] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      const [dbContacts, dbMessages] = await Promise.all([getAllUserContacts(user!.id), getAllUserMessages(user!.id)]);
      setContactsOrdered(dbContacts);
      setMessagesOrdered(dbMessages);
    }

    fetchData().catch(console.error);;
  }, []);

  const openSendMessageModal = (contact?: Contact) => {
    setSendMessageModalProps({ contactName: contact?.fullName || '', initialTo: contact?.phoneNumber || '' });
    setIsSendMessageModalVisible(true);
  }

  const doSendMessage: (to: string, content: string) => Promise<void> = async (to, content) => {
    await sendMessage(user!.phoneNumber, to, content);
    setIsSendMessageModalVisible(false);
  }

  const doCreateContact: (fullName: string, phoneNumber: string) => Promise<void> = async (fullName, phoneNumber) => {
    await createContact(user!.id, fullName, phoneNumber);
    setIsAddContactModalVisible(false);
  }

  // Contexts
  const setContactsOrdered = (contactList: Contact[]) => {
    const contactListClone = [...contactList]
    contactListClone.sort(({ fullName: aFullName }, { fullName: bFullName }) => aFullName.localeCompare(bFullName));
    setContacts(contactListClone);
  }
  const contactContext = {
    contacts,
    setContacts: setContactsOrdered,
  }

  const setMessagesOrdered = (messageList: Message[]) => {
    const messageListClone = [...messageList];
    messageListClone.sort(({ createdAt: aCreatedAt }, { createdAt: bCreatedAt }) => aCreatedAt.getTime() - bCreatedAt.getTime());
    setMessages(
      messageListClone,
    );
  }
  const messagesContext = {
    messages,
    setMessages: setMessagesOrdered,
  }


  return (
    <>
      <div className="bg-neutral-50 pt-3 h-screen">
        <div className="relative bg-white border flex w-[80vw] mx-auto">
          <ContactContext.Provider value={contactContext}>
            <MessageContext.Provider value={messagesContext}>
              <Sidebar className="justify-between">
                <Contacts contacts={contacts} onSendMessage={openSendMessageModal} onCreateContact={() => setIsAddContactModalVisible(true)} />
                <Button className="shadow-md mb-3" onClick={() => openSendMessageModal()}>
                  New message
                </Button>
              </Sidebar>
              <div className="p-3 w-full overflow-auto max-h-screen pb-[6rem]">
                {children}
              </div>
              <NotificationHandler />
            </MessageContext.Provider>
          </ContactContext.Provider>
        </div>
      </div >
      {
        isSendMessageModalVisible &&
        <SendMessageModal
          onClose={() => setIsSendMessageModalVisible(false)}
          onSend={({ to, content }) => doSendMessage(to, content)} {...sendMessageModalProps}
        />
      }
      {
        isAddContactModalVisible &&
        <AddContactModal
          onClose={() => setIsAddContactModalVisible(false)}
          onSend={({ fullName, phoneNumber }) => doCreateContact(fullName, phoneNumber)}
        />
      }
    </>
  )
}

export default Dashboard

import React, { useContext, useEffect, useState } from "react"

import ContactContext, { ContactContextType } from '../context/contacts';
import MessageContext, { MessageContextType } from '../context/messages';
import UserContext, { UserContextType } from '../context/users';
import { Message } from '../@types/messages';
import { Contact } from '../@types/contacts';
import { initSocket } from '../socket';

type WSMessage = {
  type: 'message:created' | 'contact:created',
  payload: object
}

function NotificationHandler() {
  const { contacts, setContacts } = useContext<ContactContextType>(ContactContext);
  const { messages, setMessages } = useContext<MessageContextType>(MessageContext);
  const { user } = useContext<UserContextType>(UserContext);

  const [wsMessage, setWsMessage] = useState<WSMessage | null>(null);

  useEffect(() => {
    if (!user) return

    initSocket(user.id, (type, payload) => setWsMessage({ type, payload }));
  }, []);

  useEffect(() => {
    if (wsMessage) {
      switch (wsMessage.type) {
        case 'message:created':
          updateMessages((wsMessage.payload as Message & { createdAt: string }));
          break;
        case 'contact:created':
          setContacts([...contacts, wsMessage.payload as Contact]);
          break;
      }
      setWsMessage(null)
    }
  }, [wsMessage]);

  const updateMessages = (payload: Message & { createdAt: string }) => {
    const newMessage: Message = {
      ...payload,
      createdAt: new Date(payload.createdAt),
    }
    setMessages([...messages, newMessage]);
  }

  return (
    <></>
  )
}

export default NotificationHandler

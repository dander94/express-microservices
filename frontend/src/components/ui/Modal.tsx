import React, { PropsWithChildren } from "react"

import Card from './Card';

function Modal(props: PropsWithChildren<{ title: string, onClose: () => void }>) {

  const { children, title, onClose } = props

  return (
    <div className="fixed left-0 top-0 flex h-full w-full items-center justify-center bg-black bg-opacity-50 py-10">
      <Card className="max-h-full w-full max-w-xl overflow-y-auto sm:rounded-2xl bg-white">
        <div className="p-3">
          <div className="flex justify-between mb-5">
            {title && <p className="text-xl font-bold">{title}</p>}
            <button className="text-xl font-bold" onClick={onClose}>
              X
            </button>
          </div>
          {children}
        </div>
      </Card>
    </div>
  )
}

export default Modal

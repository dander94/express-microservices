import React, { PropsWithChildren } from "react"

function Message(props: PropsWithChildren<{ className?: string, isSentByUser: boolean, contactName: string, messageDate: Date, }>) {

  const { className = '', children, contactName, isSentByUser, messageDate } = props

  const messageColorClass = (isSentByUser)
    ? `bg-lime-50`
    : `bg-neutral-50`

  return (
    <div className={`${messageColorClass} rounded-sm shadow-md p-3 ${className}`}>
      <p className="font-bold mb-1">{isSentByUser ? 'To: ' : 'From: '}{contactName}</p>
      <p className="text-sm text-gray-500  pb-2 border-b mb-4">{messageDate.toLocaleString()}</p>
      {children}
    </div >
  )
}

export default Message

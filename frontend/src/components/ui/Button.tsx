import React, { PropsWithChildren, ButtonHTMLAttributes } from "react"

function Button(props: PropsWithChildren<ButtonHTMLAttributes<{}> & { size?: '' | 'sm' | 'tiny' }>) {

  const { className = '', children, size = '', ...otherProps } = props

  let sizeClass = 'px-4 py-2';

  switch (size) {
    case 'sm':
      sizeClass = 'text-sm px-3 py-2';
      break;
    case 'tiny':
      sizeClass = 'text-xs px-2 py-1';
      break;
    default:
      break;
  }

  return (
    <button
      className={`bg-indigo-500 hover:bg-indigo-400 disabled:bg-indigo-300 rounded-md text-white font-bold text-center ${sizeClass} ${className}`}
      {...otherProps}
    >
      {children}
    </button>
  )
}

export default Button

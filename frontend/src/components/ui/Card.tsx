import React, { PropsWithChildren } from "react"

function Card(props: PropsWithChildren<{ className?: string }>) {

  const { className = '', children } = props

  return (
    <div className={`border border-gray-200 rounded-sm shadow-md p-3 ${className}`}>
      {children}
    </div >
  )
}

export default Card

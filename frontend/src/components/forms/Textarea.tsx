import React, { TextareaHTMLAttributes } from "react"

function Textarea(props: TextareaHTMLAttributes<{}>) {

  const { className = '', ...otherProps } = props

  return (
    <textarea className={`border border-gray-300 rounded-sm px-2 py-1 ${className}`} {...otherProps} />
  )
}

export default Textarea

import React, { InputHTMLAttributes } from "react"

function Input(props: InputHTMLAttributes<{}>) {

  const { className = '', ...otherProps } = props

  return (
    <input className={`border border-gray-300 rounded-sm px-2 py-1 ${className}`} {...otherProps} />
  )
}

export default Input

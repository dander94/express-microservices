import React, { useState } from "react"
import Modal from "../ui/Modal"
import Input from "../forms/Input";
import Button from "../ui/Button";

function AddContactModal(props: { onClose: () => void, onSend: (form: { fullName: string, phoneNumber: string }) => void }) {
  const { onClose, onSend } = props

  const [form, setForm] = useState<{ fullName: string, phoneNumber: string }>({ fullName: '', phoneNumber: '' });

  return (
    <Modal title="Create Contact" onClose={onClose}>
      <p>Full Name</p>
      <Input
        className="w-full mt-1"
        placeholder="John Doe"
        type="text"
        onChange={(e) => setForm({ ...form, fullName: (e.target as HTMLInputElement).value })}
      />
      <p className="mt-4">Phone Number</p>
      <Input
        className="w-full mt-1"
        placeholder="+12025550196"
        type="text"
        onChange={(e) => setForm({ ...form, phoneNumber: (e.target as HTMLInputElement).value })}
      />
      <p className="text-right mt-5">
        <Button onClick={() => onSend(form)} disabled={form.phoneNumber.length !== 12 || !form.fullName}>
          Save new contact
        </Button>
      </p>
    </Modal>
  )
}

export default AddContactModal

import React, { useState } from "react"
import Modal from "../ui/Modal"
import Input from "../forms/Input";
import Textarea from "../forms/Textarea";
import Button from "../ui/Button";

function SendMessageModal(props: { contactName?: string, initialTo?: string, onClose: () => void, onSend: (form: { to: string, content: string }) => void }) {
  const { contactName = '', initialTo = '', onClose, onSend } = props

  const [form, setForm] = useState<{ to: string, content: string }>({ to: initialTo, content: '' });

  return (
    <Modal title="Send Message" onClose={onClose}>
      <p>To: {contactName} {initialTo && <>({initialTo})</>}</p>
      {
        !contactName && <Input
          className="w-full mt-1"
          placeholder="+12025550196"
          type="text"
          onChange={(e) => setForm({ ...form, to: (e.target as HTMLInputElement).value })}
        />
      }
      <Textarea
        className="resize-none w-full mt-5"
        onChange={(e) => setForm({ ...form, content: (e.target as HTMLInputElement).value })}
        placeholder="Enter your message here..."
        rows={5}
      />
      <p className="text-right mt-5">
        <Button onClick={() => onSend(form)} disabled={form.to.length !== 12 || !form.content}>
          Send message
        </Button>
      </p>
    </Modal>
  )
}

export default SendMessageModal

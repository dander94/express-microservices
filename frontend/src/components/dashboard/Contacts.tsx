import React, { useState } from 'react';

import { Contact } from "../../@types/contacts";

import Button from '../ui/Button';

import './contacts.css'

function Contacts({ contacts, onCreateContact, onSendMessage }: { contacts: Contact[], onCreateContact: () => void, onSendMessage: (contact: Contact) => void }) {

  return (
    <div>
      <h2 className="text-lg font-bold mt-2 border-b pb-1">Contacts</h2>
      <Button className="w-full my-2" size='sm' onClick={onCreateContact}>
        + Add contact
      </Button>
      <div className="flex flex-col divide-y max-h-[80v] overflow-auto">
        {
          contacts.map(
            (contact) => (
              <div className="contacts flex justify-between hover:bg-gray-50 py-3 px-2" key={contact.id}>
                {contact.fullName}
                <Button
                  className="contacts__send"
                  onClick={() => onSendMessage(contact)}
                  size="tiny"
                >
                  SEND
                </Button>
              </div>
            )
          )
        }
      </div>
    </div>
  )
}

export default Contacts

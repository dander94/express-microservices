import React, { PropsWithChildren } from "react"

function Sidebar(props: PropsWithChildren<{ className?: string }>) {
  const { className = '', children } = props;

  return (
    <div className={`w-[20vw] flex flex-col h-[95vh] border-r overflow-auto px-3 ${className}`}>
      {children}
    </div>
  )
}

export default Sidebar

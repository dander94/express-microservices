import React, { PropsWithChildren } from "react"
function LoginLayout(props: PropsWithChildren<{}>) {
  const { children } = props;

  return (
    <>
      {children}
    </>
  )
}

export default LoginLayout

import { useState } from 'react'

import { BrowserRouter as Router, Navigate, Outlet, Route, Routes } from 'react-router-dom';

import { User } from './@types/users';

import UserContext from './context/users'

import Login from './pages/Login'
import Dashboard from './components/Dashboard';
import LoginLayout from './components/login/LoginLayout';
import Messages from './pages/Messages';

function App() {
  const [user, setUser] = useState<User | null>(null);

  const userContext = {
    user,
    setUser,
  }

  const Layout = (user)
    ? Dashboard
    : LoginLayout

  return (
    <UserContext.Provider value={userContext}>
      <Layout>
        <Router>
          <Routes>
            <Route element={user ? <Outlet /> : <Navigate to="/login" />}>
              <Route element={<Messages />} path="/" />
            </Route>
            <Route element={<Login />} path="/login" />
          </Routes>
        </Router>
      </Layout>
    </UserContext.Provider>
  )
}

export default App

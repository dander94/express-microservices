import React from "react";

import { Contact } from "../@types/contacts";

export type ContactContextType = {
  contacts: Contact[],
  setContacts: (contact: Contact[]) => void,
}

const contactContext: ContactContextType = {
  contacts: [],
  setContacts: () => { },
};

export default React.createContext(contactContext);
import React from "react";

import { User } from "../../@types/users";

export type UserContextType = {
  user: User | null,
  setUser: (user: User) => void,
}

const userContext: UserContextType = {
  user: null,
  setUser: () => {},
};

export default React.createContext(userContext);
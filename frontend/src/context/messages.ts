import React from "react";

import { Message } from "../@types/messages";

export type MessageContextType = {
  messages: Message[],
  setMessages: (contact: Message[]) => void,
}

const messageContext: MessageContextType = {
  messages: [],
  setMessages: () => { },
};

export default React.createContext(messageContext);
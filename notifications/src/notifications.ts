
import { Notification } from "./@types/notifications"
import { io } from "./server";



export const onNotificationReceived: (notification: Notification) => Promise<void> = async (notification) => {
  io.in(`user_${notification.userId}`).emit(notification.name, notification.payload);
}
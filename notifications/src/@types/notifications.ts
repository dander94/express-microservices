export interface Notification {
  name: string,
  userId: number,
  payload: Object,
}
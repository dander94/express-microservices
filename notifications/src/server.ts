import express from 'express';

import http from 'http';

import server from './app'
import { Server } from 'socket.io';


const httpServer = http.createServer(server);

const PORT = 3002;

export const io = new Server(httpServer, {
  cors: {
    origin: 'http://localhost:5173'
  }
});

io.on('connection', (socket) => {
  console.log('Incoming WS connection', socket.request.url)

  const url = new URL(socket.request.url!, `http://${socket.request.headers.host}`);

  const userId = url.searchParams.get('user_id')

  if (!userId) {
    socket.disconnect(true);
  }

  socket.join(`user_${userId}`);
});

httpServer.listen(PORT, () => {
  console.log(`Server is running on localhost:${PORT}`);
});

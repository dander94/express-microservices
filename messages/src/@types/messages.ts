export interface Message {
  from: string,
  to: string,
  content: string,
};

export type CreateMessageInput = Message

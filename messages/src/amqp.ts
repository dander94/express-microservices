import amqp, { Channel, Connection } from "amqplib";
import { Message } from "./@types/messages";

let channel: Channel | undefined;

export const initRabbitMQ: (attempt: number) => Promise<boolean> = async (attempt = 0) => {
  let conn: Connection | undefined;

  if (attempt > 9) {
    console.error('Could not connect to RabbitMQ')
    return false
  }

  try {
    conn = await amqp.connect(`amqp://${process.env.RABBITMQ_HOST}`);
  } catch {
    console.log('Connection is not ready. Retring in 2 seconds. Attempt: ', attempt + 1)
    await (new Promise((resolve) => {
      setTimeout(resolve, 2000);
    }));
    return await initRabbitMQ(attempt + 1);
  }

  try {
    channel = await conn.createChannel();
  } catch (error: any) {
    console.error('Error while connecting to RabbitMQ', error);
    return false;
  }

  console.log('Connected to RabbitMQ')
  return true
}

export const sendMessage: (message: Message) => boolean = (message: Message) => {
  if (!channel) {
    throw 'RabbitMQ is not initialized'
  }

  if (!process.env.RABBITMQ_MESSAGES_QUEUE) {
    throw 'RabbitMQ Messages Queue is not configured'
  }

  return channel.sendToQueue(process.env.RABBITMQ_MESSAGES_QUEUE, Buffer.from(JSON.stringify(message)));
}
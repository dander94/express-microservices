import express from 'express';
import { CreateMessageInput, Message } from '../@types/messages';
import { isValidMessage } from '../validators/messages';
import { sendMessage } from '../amqp';


const router = express.Router();

/* GET home page. */
router.get('/', (_, res) => {
  res.send('Hello World');
});

router.post('/message/send', (req, res) => {
  const message: CreateMessageInput = req.body;
  if (!isValidMessage(message)) {
    res.status(400).send('Please, send a valid message')
  } else {
    const success = sendMessage(message)
    res.send({ success })
  }
});

module.exports = router;

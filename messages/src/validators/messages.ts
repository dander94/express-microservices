import { Message } from "../@types/messages";


export const isValidMessage: (message: Message) => boolean = (message) => {
  const { content, from, to, ...others } = (message as Message)
  return (
    typeof content === 'string' &&
    !!content &&
    typeof from === 'string' &&
    !!from &&
    typeof to === 'string' &&
    !!to &&
    !Object.keys(others).length
  )
}
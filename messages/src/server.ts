import express from 'express';

import server from './app'

const PORT = 3001;

server.listen(PORT, () => {
  console.log(`Server is running on localhost:${PORT}`);
});
